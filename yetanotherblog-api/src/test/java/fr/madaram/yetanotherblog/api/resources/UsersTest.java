package fr.madaram.yetanotherblog.api.resources;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import fr.madaram.yetanotherblog.api.view.User;
import fr.madaram.yetanotherblog.entities.BlogUser;
import fr.madaram.yetanotherblog.services.UserService;

@RunWith(MockitoJUnitRunner.class)
public class UsersTest {

	
	@InjectMocks
	private Users users;
	
	@Mock
	UserService userService;
	
	@Test
	public void testFindUsers() {
		
		//Given
		List<BlogUser> blogUserList = new ArrayList<>();
		BlogUser bob = new BlogUser();
		bob.setUsername("Bob");
		bob.setId(Long.valueOf(1));
		blogUserList.add(bob);
		
		when(userService.find()).thenReturn(blogUserList);
		
		//When
		List<User> userList = users.findUsers();
		
		//Then
		User expectedUser = new User(Long.valueOf(1), "Bob");
		assertThat(userList).hasSize(1);
		
		User user = userList.get(0);
		assertThat(expectedUser).isEqualToComparingFieldByField(user);
	}
	
}
