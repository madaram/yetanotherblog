package fr.madaram.yetanotherblog.api.resources;

import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Component;

import fr.madaram.yetanotherblog.api.view.User;
import fr.madaram.yetanotherblog.entities.BlogUser;
import fr.madaram.yetanotherblog.services.UserService;

@Component
@Path("/users")
public class Users {

	@Inject
	private UserService userService;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> findUsers() {
		
		List<BlogUser> blogUserList = userService.find();
		
		ModelMapper modelMapper = new ModelMapper();
		Type listType =	new TypeToken<List<User>>() {}.getType();

		List<User> userList = modelMapper.map(blogUserList, listType);
		
		return userList;
	}

}
