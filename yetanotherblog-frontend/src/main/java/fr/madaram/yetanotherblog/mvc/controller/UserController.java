package fr.madaram.yetanotherblog.mvc.controller;

import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import fr.madaram.yetanotherblog.configuration.logger.InjectLogger;
import fr.madaram.yetanotherblog.services.UserService;

@Controller
public class UserController {
 
	@InjectLogger
	private Logger logger;
	
	@Inject
	private UserService userService;
 
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String findUsers(Map<String, Object> model) {
 
		logger.debug("findUsers() is executed!");
 
		model.put("users", userService.find());
 
		return "users";
	}
 
	@RequestMapping(value = "//users/{id:.+}", method = RequestMethod.GET)
	public ModelAndView findUser(@PathVariable("id") Long id) {
 
		logger.debug("findUser() is executed - $id {}", id);
 
		ModelAndView model = new ModelAndView();
		model.setViewName("user");
		model.addObject("user", userService.find(id));
 
		return model;
 
	}
 
}