<html>
<body>
	<script src="jquery_dev.js"></script>
	<script>
		$.getJSON("services/message/getmessage/toto", function(data) {
			var items = [];
			console.log(data);
			console.log(JSON.stringify(data));
			$("<p>" + JSON.stringify(data) + '</p>').appendTo("body");
			$.each(data, function(key, val) {
				items.push("<li id='" + key + "'>" + val + "</li>");
			});
			$("<ul/>", {
				"class" : "my-new-list",
				html : items.join("")
			}).appendTo("body");
		});
		$.getJSON("services/restreverse/getmessage/salut", function(data) {
			var items = [];
			console.log(data);
			console.log(JSON.stringify(data));
		});
	</script>
	<h2>Hello World!</h2>
	<p>
		Please, visit <a href="services">services</a>.
	</p>
	<p>the following is taken with jquery.getJSON from restful ws <a href="services/message/getmessage/toto">/services/message/getmessage/toto</a>.
</body>
</html>
