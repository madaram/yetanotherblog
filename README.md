# Yet another blog

##TODO LIST

### Must
* create data model
* Integrer le template bootstrap (ABO)
* create Login service + UT (?)
* backoffice thymeleaf CRUD Article (?)
* Module formatage de code (ABO)
* Service article + TU (?)
* Homepage thymeleaf + menu (?)
* Page consultation article thymeleaf (?)

### Should
* Service de recherche d'article + TU (?)
* Intégration de la recherche dans l'accueil (?)
* Add spring Security (ABO)
* Changer l'image de fond (ABO)


### Would
* Create Angluar JS mobile app (MAD + ABO)
* Google analytics (ABO)
* Module social facebook twitter (ABO)

### Could
* Deploiement PaaS
* Chat box