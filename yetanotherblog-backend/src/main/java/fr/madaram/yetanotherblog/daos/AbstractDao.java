package fr.madaram.yetanotherblog.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.slf4j.Logger;

import fr.madaram.yetanotherblog.configuration.logger.InjectLogger;

@Transactional
public class AbstractDao<T> {

	@InjectLogger
	private static Logger logger;
	
	public AbstractDao(Class<T> entityClass) {
		super();
		this.entityClass = entityClass;
	}

	/** EntityManager managed by spring. */
	@PersistenceContext
	private EntityManager entityManager;

	/** Class of the entity managed by the dao. */
	private final Class<T> entityClass;

	/**
	 * @param entity
	 * @see javax.persistence.EntityManager#persist(java.lang.Object)
	 */
	public void persist(T entity) {
		entityManager.persist(entity);
	}

	/**
	 * @param entity
	 * @see javax.persistence.EntityManager#remove(java.lang.Object)
	 */
	public void remove(T entity) {
		entityManager.remove(entity);
	}

	/**
	 * @param id
	 *            entity id
	 * @return entity with expected id
	 */
	public T findById(long id) {
		return entityManager.find(entityClass, id);
	}

	/**
	 * @return list of entities
	 */
	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		logger.info("findAll");
		return entityManager.createQuery(
				String.format("from %s", getEntityName())).getResultList();
	}

	/**
	 * @return entity name
	 */
	private String getEntityName() {
		return this.entityClass.getSimpleName();
	}

	/**
	 * @return the entity class.
	 */
	Class<T> getEntityClass() {
		return entityClass;
	}

}
