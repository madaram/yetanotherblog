/**
 * 
 */
package fr.madaram.yetanotherblog.daos;

import javax.inject.Named;
import javax.transaction.Transactional;

import fr.madaram.yetanotherblog.entities.BlogUser;

@Named
@Transactional
public class BlogUserDao extends AbstractDao<BlogUser> {

	public BlogUserDao() {
		super(BlogUser.class);
	}

}
