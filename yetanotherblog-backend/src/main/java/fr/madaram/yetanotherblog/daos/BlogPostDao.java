package fr.madaram.yetanotherblog.daos;

import javax.inject.Named;
import javax.transaction.Transactional;

import fr.madaram.yetanotherblog.entities.BlogPost;

@Named
@Transactional
public class BlogPostDao extends AbstractDao<BlogPost> {

	public BlogPostDao() {
		super(BlogPost.class);
	}

}
