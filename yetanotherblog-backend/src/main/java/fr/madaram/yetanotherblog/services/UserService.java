package fr.madaram.yetanotherblog.services;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import fr.madaram.yetanotherblog.daos.BlogUserDao;
import fr.madaram.yetanotherblog.entities.BlogUser;

/**
 * Service managing users.
 *
 */
@Named
public class UserService {
	
	@Inject
	private BlogUserDao blogUserDao;

	
	public List<BlogUser> find() {
		return blogUserDao.findAll();
	}
	
	public BlogUser find(Long id) {
		return blogUserDao.findById(id);
	}
	
	public void persist(BlogUser user) {
		blogUserDao.persist(user);
	}
}
