package fr.madaram.yetanotherblog.configuration.logger;

import java.lang.reflect.Field;
import java.util.List;

import net.vidageek.mirror.dsl.Mirror;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * Spring post processor to inject logger.
 *
 */
@Component
public class LoggerPostProcessor implements BeanPostProcessor {

	/*
	 * (non-Javadoc)
	 * @see org.springframework.beans.factory.config.BeanPostProcessor#
	 * postProcessAfterInitialization(java.lang.Object, java.lang.String)
	 */
	@Override
	public Object postProcessAfterInitialization(final Object bean,
			final String beanName) throws BeansException {
		return bean;
	}

	/**
	 * Post process bean in order to inject logger if as logger field with
	 * InjectLogger annotation.
	 */
	@Override
	public Object postProcessBeforeInitialization(final Object bean,
			final String beanName) throws BeansException {
		final List<Field> fields = getAllBeanFields(bean);
		for (final Field field : fields) {
			if (hasInjectLoggerAnnotationAndIsSubClassOfLogger(field)) {
				injectSlfForjLogger(bean, field);
			}
		}
		return bean;
	}

	/**
	 * Inject logger into bean at given field.
	 * @param bean
	 *            object to analyse
	 * @return all fields / atributes that define the class
	 */
	private List<Field> getAllBeanFields(final Object bean) {
		final List<Field> fields =
				new Mirror().on(bean.getClass()).reflectAll().fields();
		return fields;
	}

	/**
	 * @param field
	 *            field to analyse
	 * @return true if is subtype of Logger and has InjectLogger annotation
	 */
	private boolean hasInjectLoggerAnnotationAndIsSubClassOfLogger(
			final Field field) {
		return Logger.class.isAssignableFrom(field.getType())
				&& new Mirror().on(field).reflect()
						.annotation(InjectLogger.class) != null;
	}

	/**
	 * @param bean
	 *            the bean that need a logger
	 * @param field
	 *            field in which to inject the logger
	 */
	private void injectSlfForjLogger(final Object bean, final Field field) {
		new Mirror().on(bean).set().field(field)
				.withValue(LoggerFactory.getLogger(bean.getClass()));
	}
}
