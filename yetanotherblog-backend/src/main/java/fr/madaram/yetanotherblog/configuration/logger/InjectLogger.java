package fr.madaram.yetanotherblog.configuration.logger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/** Tell spring to inject logger. */
@Retention(RetentionPolicy.RUNTIME)
public @interface InjectLogger {}
