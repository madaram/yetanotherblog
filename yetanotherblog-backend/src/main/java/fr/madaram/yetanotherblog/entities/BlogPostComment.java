/**
 * 
 */
package fr.madaram.yetanotherblog.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

/** Every good blog has commentaries on his posts. */
@Entity
public class BlogPostComment extends BlogAbstractEntity implements Serializable {

	/** Generated serialVersionUID. */
	private static final long serialVersionUID = -5096725528907881074L;

	/** The author who write the comment. */
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private BlogUser author;

	/** The commented post. */
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private BlogPost commentedPost;

	/** The comment text. */
	@Column(nullable = false, columnDefinition = "TEXT")
	private String content;

	/**
	 * @return the author
	 */
	public BlogUser getAuthor() {
		return author;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(BlogUser author) {
		this.author = author;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *            the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

}
