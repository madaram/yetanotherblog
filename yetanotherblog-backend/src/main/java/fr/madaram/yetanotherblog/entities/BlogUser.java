package fr.madaram.yetanotherblog.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Blog identified user. TODO must be completed
 */
@Entity
public class BlogUser extends BlogAbstractEntity implements Serializable {

	private static final long serialVersionUID = -5367815793411408013L;

	/** User pseudo. */
	@Column(unique = true, nullable = false)
	private String username;

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

}