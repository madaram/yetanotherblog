/**
 * 
 */
package fr.madaram.yetanotherblog.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * A tag is a "meaningful" keyword. Help classifying blog posts.
 */
@Entity
public class BlogTag extends BlogAbstractEntity implements Serializable {

	/** The generated serialVersionUID. */
	private static final long serialVersionUID = 8548278926230027273L;

	/** Tag label such as java EE, php... */
	@Column(unique = true, nullable = false)
	private String label;

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

}
