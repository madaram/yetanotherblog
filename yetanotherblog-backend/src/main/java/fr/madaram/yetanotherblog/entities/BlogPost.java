package fr.madaram.yetanotherblog.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

/** What would be a blog without post ? */
@Entity
public class BlogPost extends BlogAbstractEntity implements Serializable {

	/** Generated serialVersionUID. */
	private static final long serialVersionUID = 822884291658926990L;

	/**
	 * An article can be written by many authors. An author can write many
	 * articles. Hence many to many relationship.
	 */
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<BlogUser> authors;

	/** Post title. */
	@Column(unique = true, nullable = false)
	private String title;

	/** The actual post text. */
	@Column(nullable = false, columnDefinition = "TEXT")
	private String postContent;

	/** Related post tags. */
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<BlogTag> tags;

	/**
	 * A post can be in many categories, and a category can regroup many posts.
	 */
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<BlogCategory> categories;

	/** A post can be commented. */
	@OneToMany(fetch = FetchType.LAZY)
	private List<BlogPostComment> comments;

	/**
	 * @return the authors
	 */
	public List<BlogUser> getAuthors() {
		return authors;
	}

	/**
	 * @param authors
	 *            the authors to set
	 */
	public void setAuthors(List<BlogUser> authors) {
		this.authors = authors;
	}

	/**
	 * @return the categories
	 */
	public List<BlogCategory> getCategories() {
		return categories;
	}

	/**
	 * @param categories
	 *            the categories to set
	 */
	public void setCategories(List<BlogCategory> categories) {
		this.categories = categories;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the postContent
	 */
	public String getPostContent() {
		return postContent;
	}

	/**
	 * @param postContent
	 *            the postContent to set
	 */
	public void setPostContent(String postContent) {
		this.postContent = postContent;
	}

	/**
	 * @return the comments
	 */
	public List<BlogPostComment> getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            the comments to set
	 */
	public void setComments(List<BlogPostComment> comments) {
		this.comments = comments;
	}

	/**
	 * @return the tags
	 */
	public List<BlogTag> getTags() {
		return tags;
	}

	/**
	 * @param tags
	 *            the tags to set
	 */
	public void setTags(List<BlogTag> tags) {
		this.tags = tags;
	}

}
