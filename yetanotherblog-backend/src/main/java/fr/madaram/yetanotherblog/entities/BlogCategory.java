package fr.madaram.yetanotherblog.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;

@Entity
public class BlogCategory extends BlogAbstractEntity implements Serializable {

	/** The generated serialVersionUID. */
	private static final long serialVersionUID = 8548278926230027273L;

	/** Category label such as java EE, php... */
	@Column(unique = true, nullable = false)
	private String label;

	/** Sub categories of this category. */
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<BlogCategory> subcategories;

	/**
	 * @return the subcategories
	 */
	public List<BlogCategory> getSubcategories() {
		return subcategories;
	}

	/**
	 * @param subcategories
	 *            the subcategories to set
	 */
	public void setSubcategories(List<BlogCategory> subcategories) {
		this.subcategories = subcategories;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

}
