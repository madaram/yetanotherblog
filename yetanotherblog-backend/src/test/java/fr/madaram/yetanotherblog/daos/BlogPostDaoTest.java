package fr.madaram.yetanotherblog.daos;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.madaram.yetanotherblog.configuration.db.HibernateConfiguration;
import fr.madaram.yetanotherblog.entities.BlogPost;
import fr.madaram.yetanotherblog.entities.BlogTag;
import fr.madaram.yetanotherblog.entities.BlogUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = HibernateConfiguration.class)
@ActiveProfiles({"dev"})
@Transactional
public class BlogPostDaoTest {

	/** The DAO to test.*/
	@Inject
	BlogPostDao blogPostDao;

	@Test
	public void testPersist() throws Exception {

		int size = blogPostDao.findAll().size();

		BlogPost post = new BlogPost();
		List<BlogUser> authors = new ArrayList<BlogUser>();
		BlogUser blogUser = new BlogUser();
		blogUser.setUsername("max");
		authors.add(blogUser);
		post.setAuthors(authors);

		post.setPostContent("max is mad.");
		post.setTitle("The one truth.");
		blogPostDao.persist(post);

		assertThat(blogPostDao.findAll().size()).isEqualTo(size + 1);

	}

	@Test
	public void testFindById() throws Exception {
		BlogPost post = blogPostDao.findById(1);
		assertThat(post.getTags()).hasAtLeastOneElementOfType(BlogTag.class);
		assertThat(post.getAuthors())
				.hasAtLeastOneElementOfType(BlogUser.class);

	}

	@Test
	public void testFindAll() throws Exception {
		assertThat(blogPostDao.findAll()).hasAtLeastOneElementOfType(
				BlogPost.class);
	}

	/**
	 * Remove BlogPost from DB test.
	 */
	@Test
	public void testRemoveBlogPost() {
		BlogPost entity = blogPostDao.findById(1);
		int size = blogPostDao.findAll().size();
		blogPostDao.remove(entity);
		assertThat(blogPostDao.findAll().size()).isEqualTo(size - 1);

	}

}
