package fr.madaram.yetanotherblog.daos;

import static org.assertj.core.api.Assertions.assertThat;

import javax.inject.Inject;
import javax.persistence.PersistenceException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import fr.madaram.yetanotherblog.configuration.db.HibernateConfiguration;
import fr.madaram.yetanotherblog.entities.BlogUser;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = HibernateConfiguration.class)
@ActiveProfiles({"dev"})
@Transactional
public class BlogUserDaoTest {

	/** The DAO to test.*/
	@Inject
	BlogUserDao blogUserDao;

	/**
	 * Save a user blog in database.
	 */
	@Test
	public void testPersistUserBlog() {
		BlogUser entity = new BlogUser();
		entity.setUsername("max");
		int size = blogUserDao.findAll().size();
		blogUserDao.persist(entity);
		assertThat(blogUserDao.findAll().size()).isEqualTo(size + 1);

	}

	/**
	 * Save a user user blog in database without name.
	 */
	@Test(expected = PersistenceException.class)
	public void testPersistWrongBlog() {
		BlogUser entity = new BlogUser();
		blogUserDao.persist(entity);
	}

	/**
	 * Test that the associated entity class is BlogUser.
	 */
	@Test
	public void testBlogUserDao() {
		assertThat(blogUserDao.getEntityClass()).isEqualTo(BlogUser.class);
	}

	/**
	 * Test find by, data is inserted at startup, see conf
	 * (hibernate.hbm2ddl.import_files).
	 */
	@Test
	public void testFindById() {
		assertThat(blogUserDao.findById(1).getUsername()).isEqualTo("toto");
	}

	/**
	 * Test find by, data is inserted at startup, see conf
	 * (hibernate.hbm2ddl.import_files).
	 */
	@Test
	public void testFindAll() {
		assertThat(blogUserDao.findAll()).hasAtLeastOneElementOfType(
				BlogUser.class);
	}

}
